// mảng orders
var gOrderObjects = [];
var gStt = 0;
var gOrderCode;
var gId;
var gPizzaSize = ["S", "M", "L"]

const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";

// mảng pizza Type
var gPizzaType = [
  {
    typeId: "Seafood",
    typeName: "Seafood"
  },
  {
    typeId: "Hawaii",
    typeName: "HAWAII"
  },
  {
    typeId: "Bacon",
    typeName: "Bacon"
  }
];

// mảng status
var gStatus = [
  {
    id: "open",
    name: "Open"
  },
  {
    id: "confirmed",
    name: "Confirmed"
  },
  {
    id: "cancel",
    name: "Cancel"
  }
];


const ARRAY_NAME_COL = ['Stt', 'orderCode', 'kichCo', 'loaiPizza', 'idLoaiNuocUong', 'thanhTien', 'hoTen', 'soDienThoai', 'trangThai', 'action'];
//khai báo các cột
const gCOL_NO = 0;
const gCOL_ORDERCODE = 1;
const gCOL_COMBO = 2;
const gCOL_PIZZA_ID = 3;
const gCOL_DRINK_ID = 4;
const gCOL_PRICE = 5;
const gCOL_CUSTOMER_NAME = 6;
const gCOL_PHONE = 7;
const gCOL_STATUS = 8;
const gCOL_ACTION = 9;
var gSTT = 0;

// khởi tạo datatable  - chưa có data
var gOrderTable = $('#order-table').DataTable({
  columns: [
    { data: ARRAY_NAME_COL[gCOL_NO] },
    { data: ARRAY_NAME_COL[gCOL_ORDERCODE] },
    { data: ARRAY_NAME_COL[gCOL_COMBO] },
    { data: ARRAY_NAME_COL[gCOL_PIZZA_ID] },
    { data: ARRAY_NAME_COL[gCOL_DRINK_ID] },
    { data: ARRAY_NAME_COL[gCOL_PRICE] },
    { data: ARRAY_NAME_COL[gCOL_CUSTOMER_NAME] },
    { data: ARRAY_NAME_COL[gCOL_PHONE] },
    { data: ARRAY_NAME_COL[gCOL_STATUS] },
    { data: ARRAY_NAME_COL[gCOL_ACTION] },
  ],
  columnDefs: [
    //định nghĩa các cột cần hiện ra
    {
      targets: gCOL_NO, //cột STT
      className: "text-center text-primary",
      /*render: function (data, type, row, meta) {
          return meta.row + meta.settings._iDisplayStart + 1; */
      render: function () {
        gStt++;
        return gStt;
      }
    }
    ,
    {
      targets: gCOL_ACTION, //cột Action
      className: "text-center",
      defaultContent: `
                       <button id= "update-order"  <i title = 'DEATIL' class="fa-solid fa-file-circle-check" style = "cursor:pointer; border: 1px white; color: blue"></i></button> &nbsp; 
                       <button id= "delete-order" <i title = 'Delete' class="fa-solid fa-trash" style = "cursor:pointer;border: 1px white; color: red"></i></button>`,
    }
  ]
});

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onPageLoading();
//gọi hàm hiển thị danh sách đồi uống vào select (trong modal)
getDrinkList();
//gọi hàm hiển thị kích cỡ pizza select(trong modal)
getPizzaSize(gPizzaSize);
//thêm sự kiện cho nút filter
$("#btn-filter-order").on("click", function () {
  //gọi hàm thực thi filter
  onBtnFilterOrderClick();

})
//1 - C Gán sự kiện Add cho nút Add
// gán sự kiện cho nút Confirm (trên modal)
$("#btn-add-order").on("click", function () {
  onBtnAddOrderClick();
});
//gán sự kiện cho nút confirm add order
$("#create-order-modal").on("click", "#btn-order-confirm", function () {
  onBtnConfirmAddOrderClick(this);
});
//gán sự kiện cho nút cancel trên mocal add order
$("#create-order-modal").on("click", "#btn-order-clear", function () {
  onBtnClearAddOrderClick(this);
});

// 3 - U: gán sự kiện Update - khi nhấn nút edit
$("#order-table").on("click", "#update-order", function () {
  onBtnUpdateClick(this)
});

// gán sự kiện cho nút Confirm (trên modal)
$("#btn-update-confirm").on("click", function () {
  onBtnConfirmUpdateOrderlick();
});

// 4 - D: gán sự kiện Delete - khi nhấn nút Delete
$("#order-table").on("click", "#delete-order", function () {
  onBtnDeleteClick(this)
});
//gán sự kiện cho các nút trên delete order modal
$("#btn-delete-order").on("click", function () {
  onBtnConfirmDeleteOrderClick(this);
});



/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  "use strict";
  loadDataToPizzaTypeSelect(gPizzaType);
  loadDataToStatusSelect(gStatus);
  // lấy data từ server
  callAPIToGetOrder()


}

//hàm call API to get all order
function callAPIToGetOrder() {
  $.ajax({
    url: gBASE_URL,
    type: "GET",
    dataType: 'json',
    success: function (responseObject) {
      gOrderObjects = responseObject;
      DisplayOrderToTable(responseObject);
    },
    error: function (error) {
      console.assert(error.responseText);
    }
  });
}

//hàm xử lý nút filter 
function onBtnFilterOrderClick() {
  "use strict";
  //khai báo đối tượng chứa dữ liệu filter
  var vFilterPizzaType = {
    status: "",
    pizzaTypeId: "",
  };
  //B1 thu thập dữ liệu để filter
  getFilterFormData(vFilterPizzaType);
  //B2: validate (k cần)
  //B3: xử lý filter
  var vResultFilter = filterPizzaType(vFilterPizzaType);
  console.log(vResultFilter);
  //B4 hiển thị kết quả lọc
  if (vResultFilter.length > 0) {
    //hiện thông báo đã tìm được
    alert("Found " + vResultFilter.length + " results");
    //hiển thị danh sách dữ liệu tìm được ra table
    DisplayOrderToTable(vResultFilter);
  }
  else {
    alert("No result was found");
    return false;
  }
}
//hàm xử lý sự kiện khi add order
function onBtnAddOrderClick() {
  "use strict";
  console.log("Button Add order click");
  // hiển thị modal lên
  $("#create-order-modal").modal("show");
  $("#detail-select-combo").change(function () {
    $("#detail-voucher").removeAttr('disabled');
    var status = this.value;
    console.log(status);
    if (status === "S") {
      $("#detail-size-cm").val("20");
      $("#detail-ribs").val("2");
      $("#detail-salad").val("200");
      $("#detail-amount").val("2");
      $("#detail-price").val("150000");
    }
    if (status === "M") {
      $("#detail-size-cm").val("25");
      $("#detail-ribs").val("4");
      $("#detail-salad").val("300");
      $("#detail-amount").val("3");
      $("#detail-price").val("200000");
    }
    if (status === "L") {
      $("#detail-size-cm").val("30");
      $("#detail-ribs").val("8");
      $("#detail-salad").val("500");
      $("#detail-amount").val("4");
      $("#detail-price").val("300000");
    }
    if (status === "all") {
      $("#detail-size-cm").val("");
      $("#detail-ribs").val("");
      $("#detail-salad").val("");
      $("#detail-amount").val("");
      $("#detail-price").val("");
    }
  });

  //check giá trị giảm giá khi nhấn enter
  $("#detail-voucher").keypress(function (e) {
    if (e.which == 13) {
      $("#detail-discount").val("");
      var vInputVoucherID = $("#detail-voucher").val();
      var vPrice = $("#detail-price").val();
      if (vInputVoucherID != "") {  //trường hợp input voucher khác rỗng thì check
        var vPercent = callAPIToGetPerByVoucherID(vInputVoucherID);
        var vGiamGia = 0
        if (vPercent != 0) {
          var vGiamGia = (vPercent * vPrice) / 100;
          $("#detail-discount").val(vGiamGia);
        }
        else $("#detail-discount").val(0);
      }
    }
  })

  $("#detail-voucher").focusout(function (e) {
 
      $("#detail-discount").val("");
      var vInputVoucherID = $("#detail-voucher").val();
      var vPrice = $("#detail-price").val();
      if (vInputVoucherID != "") {  //trường hợp input voucher khác rỗng thì check
        var vPercent = callAPIToGetPerByVoucherID(vInputVoucherID);
        var vGiamGia = 0
        if (vPercent != 0) {
          var vGiamGia = (vPercent * vPrice) / 100;
          $("#detail-discount").val(vGiamGia);
        }
        else $("#detail-discount").val(0);
      
    }
  })

}

// Hàm xử lý sự kiện khi icon edit trên bảng đc click
function onBtnUpdateClick(paramBtnEdit) {
  "use strict";
  console.log("Button edit được click")
  // hàm dựa vào button detail (edit or delete) xác định đc id
  var vTableRow = $(paramBtnEdit).parents("tr");
  var vOrderRowData = gOrderTable.row(vTableRow).data();
  gId = vOrderRowData.id;
  gOrderCode = vOrderRowData.orderCode;

  console.log("ID lấy được: " + gId)
  console.log("Order code lấy được: " + gOrderCode)
  getOrderByCode(gOrderCode);

}

//hàm xử lý khi update trên modal
function onBtnConfirmUpdateOrderlick() {
  // khai báo đối tượng chứa user data
  var vObjectDataOrder = {
    trangThai: "",
  }

  // B1: Thu thập dữ liệu
  getUpdateOrderData(vObjectDataOrder);
  // B2: Validate update
  // B3: update order
  $.ajax({
    url: gBASE_URL + "/" + gId,
    type: "PUT",
    contentType: "application/json;charset=UTF-8",
    data: JSON.stringify(vObjectDataOrder),
    success: function (paramRes) {
      // B4: xử lý front-end
      handleUpdateOrderSuccess();
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}


//hàm xử lý sự kiện ấn icon delete 
function onBtnDeleteClick(paramBtnDelete) {
  $("#delete-order-modal").modal("show");
  //lưu thông tin order id vào biến toàn cục
  // hàm dựa vào button detail (edit or delete) xác định đc id
  var vTableRow = $(paramBtnDelete).parents("tr");
  var vOrderRowData = gOrderTable.row(vTableRow).data();
  gId = vOrderRowData.id;
  gOrderCode = vOrderRowData.orderCode;
  console.log("Id lấy được (delete): " + gOrderCode);
}

//hàm call API to delete order
function onBtnConfirmDeleteOrderClick() {
  "use strict";
  //gọi API xóa user
  callAPIToDeleteOrder(gOrderCode);
}


//Hàm xử lý khi nhấn nút confirm trên modal
function onBtnConfirmAddOrderClick(param) {
  console.log("Button confirm add order");
  // khai báo đối tượng chứa user data
  var vObjectDataOrder = {
    kichCo: "",
    duongKinh: "",
    suon: "",
    salad: "",
    loaiPizza: "",
    idVourcher: "",
    thanhTien: "",
    giamGia: "",
    idLoaiNuocUong: "",
    soLuongNuoc: "",
    hoTen: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    loiNhan: "",
    trangThai: "",
    //ngayTao: "",
  }

  // B1: Thu thập dữ liệu
  getCreateOrderData(vObjectDataOrder);
  console.log("Thu thập thành công: " + vObjectDataOrder.giamGia)
  // B2: Validate update
  var vIsValidate = validateDataToCreate(vObjectDataOrder);
  if (vIsValidate) {
    // B3: update order
    $.ajax({
      url: "http://203.171.20.210:8080/devcamp-pizza365/orders",
      type: "POST",
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(vObjectDataOrder),
      success: function (paramRes) {
        // B4: xử lý front-end
        handleCreateOrderSuccess(paramRes.orderCode);
      },
      error: function (paramErr) {
        alert("Can't create order. Please try again")
        console.log(paramErr.status);
      }
    });
  }

}

//Hàm xử lý khi nhấn nút cancel trên modal update
function onBtnCancelOrderlick() {
  // khai báo đối tượng chứa user data
  var vObjectDataOrder = {
    trangThai: "cancel",
    ngayCapNhat: ""
  }

  // B1: Thu thập dữ liệu
  // B2: Validate update
  // B3: Update status to Cancel order
  $.ajax({
    url: gBASE_URL + "/" + gId,
    type: "PUT",
    contentType: "application/json;charset=UTF-8",
    data: JSON.stringify(vObjectDataOrder),
    success: function (paramRes) {
      // B4: xử lý front-end
      handleCancelOrderSuccess();
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}

//hàm clear trên create modal
function onBtnClearAddOrderClick(param){
  console.log("Click clear");
  $("input:text").val("");
  $("#detail-voucher").val("");
  $("#detail-select-combo").prop('selectedIndex', 0);
  $("#detail-select-pizza-type").prop('selectedIndex', 0);
  $("#detail-drink").prop('selectedIndex', 0);
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm thu thập dữ liệu để lọc
function getFilterFormData(paramFilterObj) {
  "use strict";
  paramFilterObj.pizzaTypeId = $("#gPizzaTypeSelect").val();
  paramFilterObj.status = $("#gStatusSelect").val();
  console.log("Pizza type đã lọc là: " + paramFilterObj.pizzaTypeId + " + Status: " + paramFilterObj.status);
}


//hàm thực hiện lọc và hiển thị lên table
function filterPizzaType(paramFilterObj) {
  var vResult = [];
  //thực hiện lọc
  vResult = gOrderObjects.filter(function (paramOrder) {
    if (paramOrder.loaiPizza != null) {
      return ((paramFilterObj.pizzaTypeId === "All" || paramOrder.loaiPizza.trim().toLowerCase() === paramFilterObj.pizzaTypeId.trim().toLowerCase())
        && (paramFilterObj.status === "All" || paramOrder.trangThai.trim().toLowerCase() === paramFilterObj.status.trim().toLowerCase()))
    }
  });

  return vResult;
}


//hàm load dữ liệu vào user table
function DisplayOrderToTable(paramData) {
  "use strict";
  gStt = 0; // thiết lập lại stt
  $('#order-table').DataTable().clear(); // xóa dữ liệu
  $('#order-table').DataTable().rows.add(paramData); //thêm các dòng dữ liệu vào datatable
  $('#order-table').DataTable().draw();  //vẽ lại bảng

}


// hàm show order obj lên modal
function showOrderDataToUpdateModal(paramOrderObj) {
  $("#update-detail-order-code").val(paramOrderObj.orderCode);
  $("#update-pizza-size").val(paramOrderObj.kichCo);
  $("#update-select-status").val(paramOrderObj.trangThai);
  //$("#detail-size-cm").val(paramOrderObj.duongKinh);
  //$("#detail-ribs").val(paramOrderObj.suon);
  //$("#detail-salad").val(paramOrderObj.salad);
  $("#update-pizza-type").val(paramOrderObj.loaiPizza);
  $("#update-voucher").val(paramOrderObj.idVourcher);
  $("#update-price").val(paramOrderObj.thanhTien);
  $("#update-discount").val(paramOrderObj.giamGia);
  //$("#detail-drink").val(paramOrderObj.idLoaiNuocUong);
  //$("#detail-amount").val(paramOrderObj.soLuongNuoc);
  $("#update-fullname").val(paramOrderObj.hoTen);
  $("#update-email").val(paramOrderObj.email);
  $("#update-phone").val(paramOrderObj.soDienThoai);
  $("#update-address").val(paramOrderObj.diaChi);
  $("#update-message").val(paramOrderObj.loiNhan);
  $("#update-order-date").val(paramOrderObj.ngayTao);
  //$("#detail-update-date").val(paramOrderObj.ngayCapNhat);
}

// hàm thu thập dữ liệu để create order
function getCreateOrderData(paramOrderObj) {
  paramOrderObj.kichCo = $("#detail-select-combo").val()
  paramOrderObj.duongKinh = $("#detail-size-cm").val();
  paramOrderObj.suon = $("#detail-ribs").val();
  paramOrderObj.salad = $("#detail-salad").val();
  paramOrderObj.loaiPizza = $("#detail-select-pizza-type").val();
  paramOrderObj.idVourcher = $("#detail-voucher").val().trim();
  paramOrderObj.thanhTien = $("#detail-price").val();
  paramOrderObj.giamGia = $("#detail-discount").val();
  paramOrderObj.idLoaiNuocUong = $("#detail-drink").val();
  paramOrderObj.soLuongNuoc = $("#detail-amount").val();
  paramOrderObj.hoTen = $("#detail-fullname").val();
  paramOrderObj.email = $("#detail-email").val().trim();
  paramOrderObj.soDienThoai = $("#detail-phone").val().trim();
  paramOrderObj.diaChi = $("#detail-address").val().trim();
  paramOrderObj.loiNhan = $("#detail-message").val();
  paramOrderObj.trangThai = "open";
  //paramOrderObj.ngayTao = new Date().toJSON().slice(0, 10);
}

// hàm thu thập dữ liệu để update order
function getUpdateOrderData(paramOrderObj) {
  paramOrderObj.trangThai = $("#update-select-status").val();
}

// hàm validate data khi create
function validateDataToCreate(paramOrderObj) {
  if (paramOrderObj.kichCo === "all") {
    alert("Please select Combo");
    return false;
  }
  if (paramOrderObj.loaiPizza === "all") {
    alert("Please select Pizza Type");
    return false;
  }

  if (paramOrderObj.idLoaiNuocUong === "all") {
    alert("Please select Drink");
    return false;
  }

  if (paramOrderObj.hoTen === "") {
    alert("Please input fullname");
    return false;
  }

  var vRegexStr = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (paramOrderObj.email == "") {
    alert("Please input email");
    return false;
  }

  if (!vRegexStr.test(paramOrderObj.email)) {
    alert("Invalid email!");
    $('#inp-password').focus();
    return false;
  }


  if (paramOrderObj.soDienThoai === "") {
    alert("Please input Phone Number");
    return false;
  }
  if (isNaN(paramOrderObj.soDienThoai)) {
    alert("Phone number is not correct");
    return false;
  }


  if (paramOrderObj.diaChi === "") {
    alert("Please input Adrress");
    return false;
  }

  /*if (paramOrderObj.idVourcher != "" && (isNaN(paramOrderObj.idVourcher))) {
    alert("Voucher ID must be number");
    return false;
  }*/

  return true;
}

// hàm get voucher by id
function getOrderByCode(paramOrderCode) {
  $.ajax({
    url: gBASE_URL + "/" + paramOrderCode,
    type: "GET",
    success: function (paramOrderRes) {
      showOrderDataToUpdateModal(paramOrderRes);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
  // hiển thị modal lên
  $("#update-order-modal").modal("show");
}



// hàm xử lý sự kiện confirm delete
function callAPIToDeleteOrder() {
  $.ajax({
    url: gBASE_URL + "/" + gId,
    type: "DELETE",
    success: function (paramRes) {
      handleDeleteOrderSuccess();
    },
    error: function (paramErr) {
      alert("Can not delete this user");
      $("#delete-order-modal").modal("hide");
    }
  });
}


//gọi hàm tính % giảm giá
function callAPIToGetPerByVoucherID(paramVoucherID) {
  "use strict";
  var vPercent = 0;
  $.ajax({
    url: "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail" + "/" + paramVoucherID,
    dataType: "json",
    type: "GET",
    async: false,
    success: function (paramRes) {
      console.log(paramRes);
      vPercent = paramRes.phanTramGiamGia;
      console.log(paramRes);
      return vPercent;
    },
    error: function (paramErr) {
      console.log("Không tìm thấy voucher" + paramErr);
      alert("Voucher is not available");
    }
  });
  return vPercent;
}

//hàm confirm create thành công
function handleCreateOrderSuccess(paramOrderCode) {
  alert("Created order successfully!\n Your Order Number is " + paramOrderCode);
  callAPIToGetOrder();
  resertDetailOrderForm();
  $("#create-order-modal").modal("hide");
}

//hàm confirm update thành công
function handleUpdateOrderSuccess() {
  alert("Updated order successfully!");
  callAPIToGetOrder();
  resertDetailOrderForm();
  $("#update-order-modal").modal("hide");
}

//hàm cancel thành công
function handleCancelOrderSuccess() {
  alert("Canceled order successfully!");
  callAPIToGetOrder();
  resertDetailOrderForm();
  $("#detail-order-modal").modal("hide");
}

//hàm xử lý khi delete thành công
function handleDeleteOrderSuccess() {
  alert("Deleted order successfully!");
  callAPIToGetOrder();
  resertDetailOrderForm();
  $("#delete-order-modal").modal("hide");
}

// hàm xóa trắng form detail modal
function resertDetailOrderForm() {
  $("#input-detail-order-code").val("");
  $("#detail-select-combo").val("");
  $("#detail-size-cm").val("");
  $("#detail-ribs").val("");
  $("#detail-salad").val("");
  $("#detail-pizza-type").val("");
  $("#detail-voucher").val("");
  $("#detail-price").val("");
  $("#detail-discount").val("");
  $("#detail-drink").val("");
  $("#detail-amount").val("");
  $("#detail-fullname").val("");
  $("#detail-email").val("");
  $("#detail-phone").val("");
  $("#detail-address").val("");
  $("#detail-message").val("");
  $("#detail-order-date").val("");
  $("#detail-update-date").val("");
}



//hàm load dữ liệu pizza type select
function loadDataToPizzaTypeSelect(paramData) {
  "use strict";
  $("#gPizzaTypeSelect").append($("<option>", {
    text: "All",
    value: "All"
  }));

  for (var bI = 0; bI < paramData.length; bI++) {
    $("#gPizzaTypeSelect").append($("<option>", {
      text: paramData[bI].typeName,
      value: paramData[bI].typeId
    }));
  }
  //load pizza type cho create modal
  for (var bI = 0; bI < paramData.length; bI++) {
    $("#detail-select-pizza-type").append($("<option>", {
      text: paramData[bI].typeName,
      value: paramData[bI].typeId
    }));
  }
}


//hàm load dữ liệu status select
function loadDataToStatusSelect(paramData) {
  "use strict";
  $("#gStatusSelect").append($("<option>", {
    text: "All",
    value: "All"
  }));

  for (var bI = 0; bI < paramData.length; bI++) {
    $("#gStatusSelect").append($("<option>", {
      text: paramData[bI].name,
      value: paramData[bI].id
    }));
  }
  //load select status update modal
  for (var bI = 0; bI < paramData.length; bI++) {
    $("#update-select-status").append($("<option>", {
      text: paramData[bI].name,
      value: paramData[bI].id
    }));
  }

}

//hàm gọi API select drink
function getDrinkList() {
  "use strict";
  $.ajax({
    url: "http://203.171.20.210:8080/devcamp-pizza365/drinks",
    dataType: "json",
    type: "GET",
    success: function (paramRes) {
      console.log(paramRes)
      handleDrinkList(paramRes);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    }
  });
}



//hàm tạo option cho select drink
function handleDrinkList(paramDrink) {
  "use strict";
  for (var bI = 0; bI < paramDrink.length; bI++) {
    $("#detail-drink").append($("<option>", {
      text: paramDrink[bI].tenNuocUong,
      value: paramDrink[bI].maNuocUong
    }));
  }
}

//hàm tạo option cho select size
function getPizzaSize(paramPizza) {
  "use strict";
  for (var bI = 0; bI < paramPizza.length; bI++) {
    $("#detail-select-combo").append($("<option>", {
      text: paramPizza[bI],
      value: paramPizza[bI]
    }));
  }
}


